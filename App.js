import { StatusBar } from "expo-status-bar";
import { View, Text } from 'react-native'
import LoginScreen from './component/Login';
import { NavigationContainer } from "@react-navigation/native";
import UserContext from "./component/useContext/UserContext";
import { UserInformation } from "./component/useContext/UserContext";
import StackNavigator from "./navigation/StackNavigator";
import DrawerNavigation from "./navigation/DrawerNavigation";

export default function App() {
  return (
    <NavigationContainer>
      <UserInformation>
        {/* <UserContext.Provider value='12345'> */}
        <DrawerNavigation/>
      </UserInformation>
      {/* </UserContext.Provider> */}
    </NavigationContainer>
  );
}
