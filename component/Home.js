import React, { useState, useEffect } from 'react';
import {View, Text, Button, TouchableHighlight, TouchableOpacity, StyleSheet, Alert} from 'react-native'
// imr
// ffc
import * as Notifications from 'expo-notifications';
import Exercise1 from './useState/exercise1';
import Exercise2 from './useState/exercise2';
import Exercise3 from './useState/exercise3';
import AsyncStorage from '@react-native-async-storage/async-storage';
function HomeScreen({navigation}) {
    const [name, setName] = useState(false)
    const [count, setCount] = useState(0)
    const [notiId, setNotiId] = useState(null)
    // useEffect(() => {
    //     setTimeout( ()=> {
    //         setCount(count + 1)
    //     }, 5000)
    //     //   Alert.alert('useEffect')
    // }, [])

    useEffect(() => {
        console.log('useEffect')
        Notifications.setNotificationHandler({
            handleNotification: async () => ({
              shouldShowAlert: true,
              shouldPlaySound: true,
              shouldSetBadge: true,
            }),
          });
        //  deed tald irsen notification
      const respondNoti =   Notifications.addNotificationResponseReceivedListener(responseNoti => {
            console.log('response===>', responseNoti)
            navigation.navigate('Login')
        })
      const receivedNoti =   Notifications.addNotificationReceivedListener(reseive=> {
            console.log('resive -->>', reseive)
            Alert.alert('Attention', reseive?.request?.content?.title, [{
                text: 'Go',
                onPress: () => {
                    navigation.navigate('Login')
                }
            },{
                text: 'Remove',
                onPress: () => {
                    console.log('remove')
                }
            }
            ])
        })
        Notifications.scheduleNotificationAsync({
            content: {
              title: "Test3!",
              body: 'Delgerengui!',
            },
            trigger: {
              seconds: 5,
            },
          }).then(id =>  {
            setNotiId(id)
          }
          ).catch(err => console.log(err))
       
          return () => {
            receivedNoti.remove()
            respondNoti.remove()
          }
    }, [])
    
    console.log('useEffect2', notiId)
    const changeName = () => {
        console.log('press')
        setName(!name)
        // !true --> false
        // !false --> true
    }
    return (  
        <View>
            {/* {
                console.log('name-->', name)
            } */}
            {/* <TouchableOpacity onPress={changeName}>
                <Text style={styles.name} >{name ? 'Uranchimeg' : 'Bat'}</Text>
            </TouchableOpacity> */}
         
            {/* {
                console.log('Props ---->', props.navigation)
            } */}
            {/* <Text style={styles.text}>{count}</Text> */}
            {/* {
                !name ? <Button 
                title='LoginScreen'
                color='#841584'
                onPress={() => navigation.navigate('Login')}
               />
               : null
            } */}
            
             <TouchableHighlight 
            onPress={() => navigation.navigate('Login')}
            style={styles.button}>
                 <Text style={styles.text}>TouchableHigh</Text>
            </TouchableHighlight>
            <TouchableOpacity style={styles.button}
               onPress={() => navigation.navigate('Login')}
            >
                 <Text style={styles.text}>Login</Text>
            </TouchableOpacity> 
            {/* <Exercise1/>
            <Exercise2/>
            <Exercise3/> */}
        </View>
    );
}
const styles = StyleSheet.create({
    button: {
        backgroundColor: "#DDDDDD",
        height: 50,
        marginTop: 20
    },
    text: {
        fontSize: 40
    },
    name: {
        fontSize: 40
    }
})
export default HomeScreen;