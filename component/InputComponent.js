import React from 'react'
import {View, Text, StyleSheet, TextInput} from 'react-native'


function InputComponent({placeName, value, setValue}) {
    return ( 
        <View style={styles.container}>
             <TextInput
             style={styles.input}
             placeholder= {placeName}
             value={value}
             onChangeText={(text) => setValue(text)}
             placeholderTextColor='white'
             />
        </View>
     );
}

const styles = StyleSheet.create({
     container: {
        backgroundColor:'gray',
        width: '90%',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#e8e8e8',
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginVertical: 5
     },
     input: {
        fontSize: 20,
     }
})
export default InputComponent;