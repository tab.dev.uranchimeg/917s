import React, {useState, useContext} from 'react';
import { View, Stylesheet, Text, Button } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserContext from '../useContext/UserContext';
function Register({route}) {
       console.log('route--->', route?.params?.name)
    // AsyncStorage.multiGet(["@BarcodeList", "@ScannedBarcode"]).then(response => {
    const  [asyncName, setAsyncName] = useState('')
    const value = AsyncStorage.getItem('userName').then(result => {
        setAsyncName(result)
    }).catch(err => console.log('error'))

    const removeAsync = () => {
        AsyncStorage.removeItem('userName').then(result => {
            setAsyncName('')
        }).catch(err => console.log(err))
    }
    const value2 = useContext(UserContext)
//    console.log('Value2 --->', value2)
   const changeGlobalName = () => {
      value2?.setName('Tuya')
      value2?.login(12, 45)
   }
    return ( 
        <View>
            <Text style={{fontSize: 40}}>{route?.params?.name}</Text>
            <Text style={{fontSize: 40}}>{route?.params?.key}</Text>
            <Button
            title ='Remove'
            // onPress={removeAsync}
            onPress={changeGlobalName}
            />
        </View>
     );
}

export default Register;