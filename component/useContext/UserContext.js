import React, { useState } from 'react';


const userContext = React.createContext();

// export default React.createContext();

export const UserInformation = props => {
    const [userName, setUserName] = useState(false)
    const [name, setName] = useState('Bold')
    const login = (one , two) => {
         console.log('one, two', one , two)
    }

    // const userLogin = (email, password) => {
    //     axios.post('url', {
    //         email: email,
    //         password: password
    //     }).then(result => {
    //           {result?.data}
    //           setName(result?.data)
    //     }).catch(err => {

    //     })
    // }
    return(
        <userContext.Provider value={{userName, setUserName, login, name, setName}}>
                {props?.children}
        </userContext.Provider>
    )
}

export default userContext