import React from 'react'
import { createDrawerNavigator } from "@react-navigation/drawer";
import LoginScreen from '../component/Login';
import Register from '../component/screen/Register';
import StackNavigator from './StackNavigator';
const Drawer = createDrawerNavigator();


export default () => (
    <Drawer.Navigator initialRouteName='Stack'>
          <Drawer.Screen name="Login" component={LoginScreen}/>
          <Drawer.Screen name="Stack" component={StackNavigator}/>
          <Drawer.Screen name="Register" component={Register}/>
    </Drawer.Navigator>
)