import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from '../component/screen/HomeScreen';
import Register from '../component/screen/Register';
import Home from '../component/Home';
import LoginScreen from '../component/Login';
import { SimpleLineIcons } from '@expo/vector-icons';
import DrawerNavigation from './DrawerNavigation';
const Stack = createNativeStackNavigator();
export default () => (
    <Stack.Navigator initialRouteName='Home'>
    <Stack.Screen name="Home" component={Home} 
    options={{
      headerShown: false
      // header: { visible: false }
    }}
     />
    <Stack.Screen name="Login" component={LoginScreen}
     screenOptions={{
      headerShown: false
    }} 
    options = {{
      headerShown: false,
      title:'Login change',
      headerStyle:{
        backgroundColor:'gray'
      },
      headerTintColor: 'white',
      headerTitleStyle: {
        fontSize: 30, 
        color: 'yellow'
      },
      headerRight: () => <Button title='Menu'/>,
      // headerLeft: () => {DrawerNavigation}
    //   headerLeft: () => <SimpleLineIcons name="menu" size={24} color="black" 
    // />
    }}
    />
    <Stack.Screen name="Register" component={Register} />
    <Stack.Screen name="HomeScreen" component={HomeScreen} />
</Stack.Navigator>
)